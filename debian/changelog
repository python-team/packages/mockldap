mockldap (0.3.0-8) unstable; urgency=medium

  * Team upload.
  * remove python3-mock build-dep: it's hybridized
  * use new dh-sequence-python3
  * set Rules-Requires-Root: no
  * add runtime dependency on python3-ldap

 -- Alexandre Detiste <tchet@debian.org>  Wed, 10 Apr 2024 19:53:31 +0200

mockldap (0.3.0-7) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-mockldap-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 11:47:06 +0100

mockldap (0.3.0-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 12:05:55 +0100

mockldap (0.3.0-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Mon, 13 Jun 2022 20:35:50 -0400

mockldap (0.3.0-4) unstable; urgency=medium

  * Team upload.
  * Reupload without binaries to allow migration.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 07 Aug 2019 23:24:51 +0500

mockldap (0.3.0-3) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Michael Fladischer ]
  * Add python-mock to Depends for the Python2 package (Closes: #880865).
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.4.0.
  * Remove Python2 support.

 -- Michael Fladischer <fladi@debian.org>  Tue, 30 Jul 2019 12:38:13 +0200

mockldap (0.3.0-2) unstable; urgency=medium

  * Add python-mock to Depends for the Python2 package (Closes: #880865).
  * Run wrap-and-sort -bast to reduce diff size of future changes.

 -- Michael Fladischer <fladi@debian.org>  Thu, 09 Nov 2017 22:31:24 +0100

mockldap (0.3.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches after git-dpm to gbp pq conversion
  * Move upstream signing key to debian/upstream/signing-key.asc.
  * Replace Priority extra with optional as required by policy 4.0.1.
  * Bump Standards-Version to 4.1.1.
  * Raise minimum Python3 version to 3.4.
  * Use https:// for uscan URL.
  * Refresh patches for 0.3.0.
  * Clean up modified files to allow two builds in a row.
  * Disable test until upstream includes them in their tarball again.

 -- Michael Fladischer <fladi@debian.org>  Wed, 01 Nov 2017 18:49:44 +0100

mockldap (0.2.8-1) unstable; urgency=low

  * Upload to unstable (Closes: #866016).
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Thu, 06 Jul 2017 20:52:12 +0200

mockldap (0.2.8-1~exp1) experimental; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sun, 19 Mar 2017 08:03:08 +0100

mockldap (0.2.7-1~exp1) experimental; urgency=low

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release.
  * Remove version constraint from python-sphinx in Build-Depends, no
    longer necessary.
  * Add patch to use pyldap for both Python2 and Python3.
  * Replace python-ldap with python-pyldap in Build-Depends.
  * Use https:// for copyright-format 1.0 URL.
  * Bump Standards-Version to 3.9.8.
  * Enable Python3 support through additional binary package.
  * Switch to python3-sphinx.
  * Bump minimum Python2 version to 2.7.
  * Update years in d/copyright.
  * Install README for python3-mockldap.

 -- Michael Fladischer <fladi@debian.org>  Sat, 18 Mar 2017 08:24:59 +0100

mockldap (0.2.5-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Tue, 30 Jun 2015 16:45:21 +0200

mockldap (0.2.4-1) unstable; urgency=medium

  * New upstream release.
  * Drop python3-mockldap binary package as it is not compatible with
    python3-ldap3 and there is no python3-ldap (Closes: #784645).
  * Bump Standards-Version to 3.9.6.
  * Use pypi.debian.net service for uscan.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Wed, 27 May 2015 20:54:29 +0200

mockldap (0.2.2-3) unstable; urgency=low

  * Switch buildsystem to pybuild.
  * Add dh-python to Build-Depends.
  * Add src/mockldap.egg-info/requires.txt to d/clean to allow two builds in a
    row.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 28 Aug 2014 21:56:00 +0200

mockldap (0.2.2-2) unstable; urgency=medium

  * Use correct package for python_ldap in Python3 by providing
    d/py3dist-overrides.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Wed, 27 Aug 2014 15:00:51 +0200

mockldap (0.2.2-1) unstable; urgency=low

  * New upstream release.
  * Add Python3 support. Test are not yet compatible.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 21 Aug 2014 11:40:02 +0200

mockldap (0.2.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Wed, 23 Apr 2014 12:36:21 +0200

mockldap (0.1.8-1) unstable; urgency=medium

  * New upstream release (Closes: #741783).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Tue, 01 Apr 2014 16:22:58 +0200

mockldap (0.1.7-1) unstable; urgency=low

  * New upstream release.
  * Check PGP signature on upstream tarball:
    + Add signature URL to debian/watch.
    + Include upstream public PGP key D0FA0E76.
    + Allow debian/upstream-signing-key.pgp to be included as a binary.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Tue, 11 Mar 2014 22:10:50 +0100

mockldap (0.1.4-1) unstable; urgency=low

  * Initial release (Closes: #730530)

 -- Michael Fladischer <FladischerMichael@fladi.at>  Tue, 03 Dec 2013 11:54:20 +0100
